import { InterviewFrontEndPage } from './app.po';

describe('interview-front-end App', () => {
  let page: InterviewFrontEndPage;

  beforeEach(() => {
    page = new InterviewFrontEndPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
