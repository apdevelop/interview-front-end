import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { MapComponent } from './map/map.component';
import {AgmCoreModule} from 'angular2-google-maps/core';
import {AngularFireModule} from 'angularfire2';
import {MapService} from './map.service';
import {CfgService} from './cfg.service';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {MapHttpService} from './map-http.service';
import { MapAnsComponent } from './map-ans/map-ans.component';
import {MapAnsService} from 'app/map-ans.service';
import {MapHttpAnsService} from 'app/map-http-ans.service';

export const firebaseConfig = {
  apiKey: "AIzaSyDEO6t8feZEBPEWPKNpSjjmD74Vv5S2uAw",
  authDomain: "fe-tests.firebaseapp.com",
  databaseURL: "https://fe-tests.firebaseio.com",
  projectId: "fe-tests",
  storageBucket: "fe-tests.appspot.com",
  messagingSenderId: "976842114899",
  appId: "1:976842114899:web:2dff59125fe30b27"
};


@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    MapAnsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAtQtj9eP0ma3wQk0qAFZ8xsDZKzwHvKd0'
    }),
    HttpModule
  ],
  providers: [
    MapService,
    MapHttpService,
    CfgService,
    MapAnsService,
    MapHttpAnsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
