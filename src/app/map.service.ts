import {Injectable} from '@angular/core';
import {AngularFireDatabase, FirebaseListObservable} from "angularfire2/database";
import {CfgService} from "./cfg.service";

@Injectable()
export class MapService {

  roadIncidents: FirebaseListObservable<any>;

  constructor(private db: AngularFireDatabase, private cfgs: CfgService) {
    this.roadIncidents = db.list(cfgs.getDbPath() + '/roadIncidents');

    // Question 1:
    // -----------
    // "riskAlerts"
  }


  getRoadIncidents() {
    return this.roadIncidents;
  }


  // Question 1:
  // -----------
}
