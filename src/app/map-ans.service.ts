import { Injectable } from '@angular/core';
import {AngularFireDatabase, FirebaseListObservable} from "angularfire2/database";
import {CfgService} from "./cfg.service";

@Injectable()
export class MapAnsService {

  roadIncidents: FirebaseListObservable<any>;
  riskAlerts: FirebaseListObservable<any>;

  constructor(private db: AngularFireDatabase, private cfgs: CfgService) {
    this.roadIncidents = db.list(cfgs.getDbPath() + '/roadIncidents');

    // Question 1:
    // -----------
    this.riskAlerts = db.list(cfgs.getDbPath() + '/riskAlerts');
  }


  getRoadIncidents() {
    return this.roadIncidents;
  }


  // Question 1:
  // -----------
  getRiskAlerts() {
    return this.riskAlerts;
  }
}
