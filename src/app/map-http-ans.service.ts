import { Injectable } from '@angular/core';
import {Http} from '@angular/http';

@Injectable()
export class MapHttpAnsService {


  // API -
  // Request - https://m-demo-238f6.firebaseio.com/cityName.json
  // response - cityName
  constructor(private http: Http) { }

  getCityName() {

    // Question 2:
    // -----------
    return this.http
      .get('https://m-demo-238f6.firebaseio.com/cityName.json');

  }
}
