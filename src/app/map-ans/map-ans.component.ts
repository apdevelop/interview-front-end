import { Component, OnInit } from '@angular/core';
import {FirebaseListObservable} from 'angularfire2/database';
import {MapAnsService} from '../map-ans.service';
import {Response} from '@angular/http';
import {MapHttpAnsService} from '../map-http-ans.service';

@Component({
  selector: 'app-map-ans',
  templateUrl: './map-ans.component.html',
  styleUrls: ['./map-ans.component.css']
})
export class MapAnsComponent implements OnInit {

  mapControl = {
    type: 'none',
    id: -1,
    center: {
      lat: 36.136507,
      lng: -115.162020
    },
    zoom: 12
  };

  cityName;

  roadIncidents: FirebaseListObservable<any>;
  riskAlerts: FirebaseListObservable<any>;

  constructor(private _mS: MapAnsService, private _mHS: MapHttpAnsService) {
    this.roadIncidents = _mS.getRoadIncidents();

    // Question 1:
    // -----------
    this.riskAlerts = _mS.getRiskAlerts();

    // Question 2:
    // -----------
    _mHS.getCityName().subscribe(
      (res: Response) => {
        console.log(res.json());
        this.cityName = res.json();
      },
      (error) => {
        console.log(error);
      }
    );

  }


  ngOnInit() {
  }
}
