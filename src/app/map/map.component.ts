import { Component, OnInit } from '@angular/core';
import {FirebaseListObservable} from 'angularfire2/database';
import {MapService} from '../map.service';
import {MapHttpService} from '../map-http.service';
import {Response} from '@angular/http';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  mapControl = {
    type: 'none',
    id: -1,
    center: {
      lat: 36.136507,
      lng: -115.162020
    },
    zoom: 12
  };

  cityName;

  roadIncidents: FirebaseListObservable<any>;

  constructor(private _mS: MapService, private _mHS: MapHttpService) {
    this.roadIncidents = _mS.getRoadIncidents();

    // Question 1:
    // -----------

    // Question 2:
    // -----------

  }


  ngOnInit() {
  }

}
